package ru.spovetkin.demo.kafkademo.service;

import ru.spovetkin.demo.kafkademo.model.Index;
import ru.spovetkin.demo.kafkademo.model.Message;

public interface KafkaService {

    void sendMessage(Message message);

    void sendIndex(Index index);

}
