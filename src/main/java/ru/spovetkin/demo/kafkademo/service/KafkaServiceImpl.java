package ru.spovetkin.demo.kafkademo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import ru.spovetkin.demo.kafkademo.model.Index;
import ru.spovetkin.demo.kafkademo.model.Message;

@Service
public class KafkaServiceImpl implements KafkaService {

    private KafkaTemplate<String, Message> messageKafkaTemplate;
    private KafkaTemplate<String, Index> indexKafkaTemplate;


    @Autowired
    public KafkaServiceImpl(KafkaTemplate<String, Message> messageKafkaTemplate,
                            KafkaTemplate<String, Index> indexKafkaTemplate) {
        this.messageKafkaTemplate = messageKafkaTemplate;
        this.indexKafkaTemplate = indexKafkaTemplate;
    }

    @Override
    public void sendMessage(Message message) {
        messageKafkaTemplate.send("messagetopic", message);
    }

    @Override
    public void sendIndex(Index index) {
        indexKafkaTemplate.send("indextopic", index);
    }

}
