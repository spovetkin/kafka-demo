package ru.spovetkin.demo.kafkademo.listener;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import ru.spovetkin.demo.kafkademo.model.Index;
import ru.spovetkin.demo.kafkademo.model.Message;

@Component
public class ClientKafkaListener {

    @KafkaListener(topics = "messagetopic", containerFactory = "messageKafkaListenerContainerFactory")
    public void messageListener(Message message) {
        System.out.println("Consume: " + message.toString());
    }

    @KafkaListener(topics = "indextopic", containerFactory = "indexKafkaListenerContainerFactory")
    public void indexListener(Index index) {
        System.out.println("Consume: " + index.toString());
    }

}
