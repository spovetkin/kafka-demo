package ru.spovetkin.demo.kafkademo.model;

public class Index {

    private String objectId;
    private String objectType;

    public Index() {
    }

    public Index(String objectId, String objectType) {
        this.objectId = objectId;
        this.objectType = objectType;
    }

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getObjectType() {
        return objectType;
    }

    public void setObjectType(String objectType) {
        this.objectType = objectType;
    }

    @Override
    public String toString() {
        return "Index{" +
                "objectId='" + objectId + '\'' +
                ", objectType='" + objectType + '\'' +
                '}';
    }
}
