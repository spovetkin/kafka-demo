package ru.spovetkin.demo.kafkademo.model;

public class Message {

    private String message;
    private String messageType;
    private int number;

    public Message(String message, String messageType, int number) {
        this.message = message;
        this.messageType = messageType;
        this.number = number;
    }

    public Message() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "Message{" +
                "message='" + message + '\'' +
                ", messageType='" + messageType + '\'' +
                ", number=" + number +
                '}';
    }
}
