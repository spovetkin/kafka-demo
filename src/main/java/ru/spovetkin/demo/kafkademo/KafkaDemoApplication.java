package ru.spovetkin.demo.kafkademo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.spovetkin.demo.kafkademo.model.Index;
import ru.spovetkin.demo.kafkademo.model.Message;
import ru.spovetkin.demo.kafkademo.service.KafkaService;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class KafkaDemoApplication {

    private KafkaService kafkaService;

    @Autowired
    public KafkaDemoApplication(KafkaService kafkaService) {
        this.kafkaService = kafkaService;
    }

    public static void main(String[] args) {
        SpringApplication.run(KafkaDemoApplication.class, args);
    }

    @PostConstruct
    public void postConstruct() {
        kafkaService.sendMessage(new Message("Message text 1", "Messge type 1", 1));
        kafkaService.sendMessage(new Message("Message text 2", "Messge type 2", 2));

        kafkaService.sendIndex(new Index("1", "DocObject"));
        kafkaService.sendIndex(new Index("2", "DocObject"));
    }

}
